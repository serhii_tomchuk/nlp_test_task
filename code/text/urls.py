from django.urls import path
from text.views import (
    CreateTextView,
    DetailTextView,
    ListTextView,
    UpdateTextView,
    DeleteTextView
)

urlpatterns = [
    path('list/', ListTextView.as_view(), name='list_text'),
    path('detail/<int:pk>', DetailTextView.as_view(), name='detail_text'),
    path('update/<int:pk>', UpdateTextView.as_view(), name='update_text'),
    path('delete/<int:pk>', DeleteTextView.as_view(), name='delete_text'),
    path('', CreateTextView.as_view(),  name='create_text'),
]


