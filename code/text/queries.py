from text.models import Parts
from text.spacy import SpacyText


def get_words_pos_tags(data):
    """Get cleaned_data from CreateTextForm
    """
    return [
        item
        for item in data
        if item["pos"] != "PUNCT" or item["pos"] != "SPACE"
    ]


def save_parts(data, text_id):
    """Save Parts model
    """
    [
        Parts(**item, **{"text_id": text_id}).save()
        for item in get_words_pos_tags(data=SpacyText(text=data).data_for_db())
    ]
