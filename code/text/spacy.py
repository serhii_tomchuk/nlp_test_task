import spacy

nlp = spacy.load("en_core_web_sm")


class SpacyText:
    def __init__(self, text):
        self.text = text

    def _prepare_data(self, text):
        return [
            {
                "word": word,
                "pos": word.pos_,
                "tag": word.tag_
            }
            for word in nlp(text)
        ]

    def data_for_db(self):
        return self._prepare_data(text=self.text)
