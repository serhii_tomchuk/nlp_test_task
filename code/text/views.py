from django.urls import reverse_lazy
from django.views.generic import (
    CreateView,
    ListView,
    DetailView,
    UpdateView,
    DeleteView
)
from text.forms import CreateTextForm
from text.models import Text, Parts
from text.queries import save_parts


class CreateTextView(CreateView):
    form_class = CreateTextForm
    template_name = 'text/create_text.html'

    def _save_parts_data(self):
        return save_parts(
            data=self.object.body,
            text_id=self.object.id
        )

    def form_valid(self, form):
        self.object = form.save()
        self._save_parts_data()
        return super(CreateTextView, self).form_valid(form)


class ListTextView(ListView):
    model = Text
    context_object_name = 'text_list'
    template_name = 'text/list_text.html'


class UpdateTextView(UpdateView):
    model = Text
    success_url = reverse_lazy('list_text')
    template_name = 'text/update_text.html'
    fields = ('body',)


class DetailTextView(DetailView):
    model = Text
    template_name = 'text/detail_text.html'


class DeleteTextView(DeleteView):
    model = Text
    success_url = reverse_lazy('list_text')
    template_name = 'text/delete_text.html'
