from django.db import models
from django.urls import reverse


class Text(models.Model):
    body = models.TextField(max_length=5000, verbose_name='Text body')

    def get_absolute_url(self):
        return reverse('detail_text', args=[str(self.pk)])


class Parts(models.Model):
    text = models.ForeignKey(
        Text, on_delete=models.CASCADE,
        verbose_name='Text',
        blank=False, null=False,
        related_name='text'
    )
    word = models.CharField(max_length=150, verbose_name='Word')
    pos = models.CharField(max_length=50, verbose_name='Part of speech')
    tag = models.CharField(max_length=50, verbose_name='Tag')
