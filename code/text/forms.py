from django import forms
from text.models import Text


class CreateTextForm(forms.ModelForm):
    class Meta:
        model = Text
        fields = ("body",)
